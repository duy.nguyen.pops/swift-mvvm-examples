# MVVM example 1

![alt text][mvvm]

Very simple, this application has only one ViewController that contains one button and a label.
Initially, button says `"Click to hide text"`, and label is visible. Whenever user touchs on button, it's text will be toggled between `"Click to hide text"` and `"Click to show text"`, and the label visibility is also updated.

In this sample, we don't care about model. Model will be discussed in next example

## Step 1: define view model protocol

First we define a protocol included all commands & notification for this view:

  - Commands:
    + toggleLabelVisibility: UI calls this commands to let viewModel know that user has touched on Button
    
  - Notifications:
    + onLabelVisibilityChanged: let UI knows that label visibility has changed.
    
  - Getters: list of getters for view-model's states

Code:


    protocol ViewControllerProtocol: class {
      //notifications
      var onLabelVisibilityChanged: ((ViewControllerProtocol, Bool) -> ())? {get set}
    
      //commands
      func toggleLabelVisibility() -> Void;
    
      //getters
      var buttonText:String? {get}
      var textVisibility:Bool? {get}
    }
  
  
## Step 2: implement view model class

ViewModel class is an implementation of view-model protocol, that defines states and commands

    class ViewControllerViewModel: ViewControllerProtocol {
      //define states
      var buttonText: String?;
      var onLabelVisibilityChanged: ((ViewControllerProtocol, Bool) -> ())?
      var textVisibility:Bool? {
        didSet {
          buttonText = textVisibility! ? "Click to hide text" : "Click to show text"
          dispatch_async(dispatch_get_main_queue(), {[unowned self] () -> Void in
            self.onLabelVisibilityChanged!(self, self.textVisibility!)
          })
        }
      }
      
      //implement commands
      func toggleLabelVisibility() {
        textVisibility = !textVisibility!;
      }
      
      required init() {
        defer {
          self.textVisibility = true
        }
      }
    }

When UI calls `toggleLabelVisibility`, `textVisibility` value will be changed, so also triggers `textVisibility:didSet`. In `didSet`

`defer` section make sure that `textVisibility:didSet` will be triggered right after view-model was inited.

#Step 3: Implememt view
It's quite easy with drag & drop feautre. The most important thing todo is we have to define view-model instance inside viewController class

    import UIKit
    class ViewController: UIViewController {
      @IBOutlet weak var myButton: UIButton!
      @IBOutlet weak var myLabel: UILabel!
      
      var viewModel:ViewControllerViewModel! {
        didSet {
          self.viewModel.onLabelVisibilityChanged = {[unowned self] (viewModel, visibility) in
            self.myLabel.hidden = !visibility
            self.myButton.setTitle(viewModel.buttonText, forState: UIControlState.Normal)
          }
        }
      }
      
      override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel = ViewControllerViewModel();
      }
      
      @IBAction func onButtonClicked(sender: AnyObject) {
        viewModel.toggleLabelVisibility()
      }
    }

First, we define viewModel variable and also implement `onLabelVisibilityChanged` notification. In this example, we update label visibility and button title follow view-model state

    var viewModel:ViewControllerViewModel! {
      didSet {
        self.viewModel.onLabelVisibilityChanged = {[unowned self] (viewModel, visibility) in
          self.myLabel.hidden = !visibility
          self.myButton.setTitle(viewModel.buttonText, forState: UIControlState.Normal)
        }
      }
    }

In the orther hand, we listen user event `onButtonClicked` to, and then pass the command `toggleLabelVisibility` to viewModel 

    @IBAction func onButtonClicked(sender: AnyObject) {
        viewModel.toggleLabelVisibility()
    }
    


[mvvm]: https://i-msdn.sec.s-msft.com/dynimg/IC564167.png "MVVM Model"