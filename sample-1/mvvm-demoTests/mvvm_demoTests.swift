//
//  mvvm_demoTests.swift
//  mvvm-demoTests
//
//  Created by NGUYEN KHANH DUY on 5/17/16.
//  Copyright © 2016 NGUYEN KHANH DUY. All rights reserved.
//

import XCTest
@testable import mvvm_demo

class mvvm_demoTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testToggleTextVisible() {
        
        let expectation = expectationWithDescription("Wait for route")
        
        var visibilityHasChangedCounter:Int = 0;
        var viewModel:ViewControllerProtocol! {
            didSet {
                viewModel.onLabelVisibilityChanged = { (viewModel, visibility) in
                    visibilityHasChangedCounter += 1;
                    if (visibilityHasChangedCounter == 2) {
                        expectation.fulfill()
                    }
                }
            }
        }
        viewModel = ViewControllerViewModel()
        XCTAssertNotNil(viewModel)
        XCTAssertTrue(viewModel.textVisibility!);
        XCTAssertTrue(viewModel.buttonText! == "Click to hide text");
        
        viewModel.toggleLabelVisibility();
        XCTAssertFalse(viewModel.textVisibility!);
        
        waitForExpectationsWithTimeout(2000) { (error) in
        }
    }
    
}
