//
//  ViewController~ViewModel.swift
//  mvvm-demo
//
//  Created by NGUYEN KHANH DUY on 5/17/16.
//  Copyright © 2016 NGUYEN KHANH DUY. All rights reserved.
//

import Foundation

protocol ViewControllerProtocol: class {
    //notifications
    var onLabelVisibilityChanged: ((ViewControllerProtocol, Bool) -> ())? {get set}
    
    //commands
    func toggleLabelVisibility() -> Void;
    
    //getters
    var buttonText:String? {get}
    var textVisibility:Bool? {get}
}


class ViewControllerViewModel: ViewControllerProtocol {
    //define states
    var buttonText: String?;
    var onLabelVisibilityChanged: ((ViewControllerProtocol, Bool) -> ())?
    var textVisibility:Bool? {
        didSet {
            buttonText = textVisibility! ? "Click to hide text" : "Click to show text"
            dispatch_async(dispatch_get_main_queue(), {[unowned self] () -> Void in
                self.onLabelVisibilityChanged!(self, self.textVisibility!)
            })
            
        }
    }
    
    //implement commands
    func toggleLabelVisibility() {
        textVisibility = !textVisibility!;
    }
    
    required init() {
        defer {
            self.textVisibility = true
        }
    }
}
