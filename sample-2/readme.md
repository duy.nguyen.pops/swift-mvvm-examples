# MVVM example 2

![alt text][mvvm]

This sample works exactly same as example 1. However, we refactor code to create a good structure

## Step 1: define common view-model protocol

    import Foundation
    protocol ViewState {}

    protocol ViewModelProtocol: class {
        var onViewStateChanged: ((ViewModelProtocol, ViewState, ViewState?) -> ())? {get set}
        func getViewState() -> ViewState;
        func setViewState(viewstate: ViewState) -> Void;
        func execute(command:Int, data:AnyObject?) -> Void;
    }

`ViewState` protocol defines base protocol view state of view
`ViewModelProcotol` defines base operations of ViewModel
  - onViewStateChanged: universal notification
  - getViewState/setViewState: get/set current state of view
  - execute: UI uses this function to send command from UI to View-Model

## Step 2: Define ViewModel for ViewController.

First, we define view state struct, contains all state of views

    struct ViewControllerViewState: ViewState {
        internal static let BTN_TEXT_HIDE:String = "Click to hide text";
        internal static let BTN_TEXT_SHOW:String = "Click to show text";
        
        let buttonText: String
        let textVisibility: Bool
        
        init(textVisibility:Bool) {
            self.textVisibility = textVisibility;
            self.buttonText = textVisibility ? ViewControllerViewState.BTN_TEXT_HIDE : ViewControllerViewState.BTN_TEXT_SHOW
        }
    }

In this example, there're only two property `buttonText` and `textVisibility`

The most important thing we have to do in this step is define `execute` function

    class ViewControllerViewModel: ViewModelProtocol {
        internal enum Command:Int {
            case ToggleVisibility = 0
        }
        
        var onViewStateChanged: ((ViewModelProtocol, ViewState, ViewState?) -> ())?
        
        var viewState:ViewControllerViewState?  {
            willSet(newState) {
                let ns:ViewState = newState as! ViewState
                let current = self.viewState as? ViewState
                dispatch_async(dispatch_get_main_queue(), {[unowned self] () -> Void in
                    self.onViewStateChanged!(self, ns, current)
                })
            }
        }
        
        func execute(command:Int, data:AnyObject?) {
            if let c:Command = Command(rawValue: command) {
                switch c {
                case .ToggleVisibility:
                    let visibility:Bool = !viewState!.textVisibility;
                    viewState = ViewControllerViewState(textVisibility: visibility)
                }
            }
        }
        
        func getViewState() -> ViewState {
            return viewState!;
        }
        
        func setViewState(vs: ViewState) {
            self.viewState = vs as? ViewControllerViewState
        }
        
        
        required init() {
            defer {
                self.viewState = ViewControllerViewState(textVisibility: true)
            }
        }
    }
    
In this example, whenever UI calls `.ToggleVisibility` command, new view state is created, then `onViewStateChanged` will be called via `viewState:didSet`

Everytime viewState has been changed, `onViewStateChanged` is triggered, and send new & old state to UI

    var viewState:ViewControllerViewState?  {
        willSet(newState) {
            let ns:ViewState = newState as! ViewState
            let current = self.viewState as? ViewState
            dispatch_async(dispatch_get_main_queue(), {[unowned self] () -> Void in
                self.onViewStateChanged!(self, ns, current)
            })
        }
    }

An important note, view state should be keep as const so that UI view can keep track new/old value. When state changed, a new state will be created and replaced old state

    viewState = ViewControllerViewState(textVisibility: visibility)


#Step 3: Implememt view
There's no many change since last version, ViewController just have to chance the way of using view model

    import UIKit
    class ViewController: UIViewController {
        @IBOutlet weak var myButton: UIButton!
        @IBOutlet weak var myLabel: UILabel!
        
        var viewModel:ViewControllerViewModel! {
            didSet {
                self.viewModel.onViewStateChanged = {[unowned self] (viewModel, newViewState, oldViewState) in
                    let v:ViewControllerViewState = newViewState as! ViewControllerViewState
                    self.myLabel.hidden = !v.textVisibility
                    self.myButton.setTitle(v.buttonText, forState: UIControlState.Normal)
                }
            }
        }

        override func viewDidLoad() {
            super.viewDidLoad()
            self.viewModel = ViewControllerViewModel();
        }
        @IBAction func onButtonClicked(sender: AnyObject) {
            viewModel.execute(ViewControllerViewModel.Command.ToggleVisibility.rawValue, data: NSNull())
        }
    }

#Step 4: Unit test


    func testSetViewState() {
        let viewModel = ViewControllerViewModel()
        XCTAssertNotNil(viewModel)
        let viewState = ViewControllerViewState(textVisibility: false) as ViewState
        viewModel.setViewState(viewState);
        
        let v:ViewControllerViewState = viewModel.getViewState() as! ViewControllerViewState
        XCTAssertFalse(v.textVisibility);
        XCTAssertTrue(v.buttonText == ViewControllerViewState.BTN_TEXT_SHOW);
    }
    
    func testToggleTextVisible() {
        var updateCounter = 0;
        
        var viewModel:ViewModelProtocol! {
            didSet {
                viewModel.onViewStateChanged = { (viewModel, newViewState, oldViewState) in
                    updateCounter += 1;
                    let _oldViewState: ViewState! = oldViewState
                    if _oldViewState != nil {
                        if (updateCounter == 2) {
                            let v:ViewControllerViewState = newViewState as! ViewControllerViewState
                            XCTAssertFalse(v.textVisibility);
                            XCTAssertTrue(v.buttonText == ViewControllerViewState.BTN_TEXT_SHOW);
                        } else if (updateCounter == 3) {
                            let v:ViewControllerViewState = newViewState as! ViewControllerViewState
                            XCTAssertTrue(v.textVisibility);
                            XCTAssertTrue(v.buttonText == ViewControllerViewState.BTN_TEXT_HIDE);
                        }
                    }
                }
            }
        }
        viewModel = ViewControllerViewModel()
        XCTAssertNotNil(viewModel)
        
        let v:ViewControllerViewState = viewModel.getViewState() as! ViewControllerViewState
        
        XCTAssertTrue(v.textVisibility);
        XCTAssertTrue(v.buttonText == ViewControllerViewState.BTN_TEXT_HIDE);
        
        viewModel.execute(ViewControllerViewModel.Command.ToggleVisibility.rawValue, data: NSNull())
        viewModel.execute(ViewControllerViewModel.Command.ToggleVisibility.rawValue, data: NSNull())
    }

[mvvm]: https://i-msdn.sec.s-msft.com/dynimg/IC564167.png "MVVM Model"