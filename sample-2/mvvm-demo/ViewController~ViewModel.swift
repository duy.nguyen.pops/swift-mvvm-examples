//
//  ViewController~ViewModel.swift
//  mvvm-demo
//
//  Created by NGUYEN KHANH DUY on 5/17/16.
//  Copyright © 2016 NGUYEN KHANH DUY. All rights reserved.
//

import Foundation

struct ViewControllerViewState: ViewState {
    internal static let BTN_TEXT_HIDE:String = "Click to hide text";
    internal static let BTN_TEXT_SHOW:String = "Click to show text";
    
    let buttonText: String
    let textVisibility: Bool
    init(textVisibility:Bool) {
        self.textVisibility = textVisibility;
        self.buttonText = textVisibility ? ViewControllerViewState.BTN_TEXT_HIDE : ViewControllerViewState.BTN_TEXT_SHOW
    }
}

class ViewControllerViewModel: ViewModelProtocol {
    internal enum Command:Int {
        case ToggleVisibility = 0
    }
    
    var onViewStateChanged: ((ViewModelProtocol, ViewState, ViewState?) -> ())?
    
    var viewState:ViewControllerViewState?  {
        willSet(newState) {
            let ns:ViewState = newState as! ViewState
            let current = self.viewState as? ViewState
            dispatch_async(dispatch_get_main_queue(), {[unowned self] () -> Void in
                self.onViewStateChanged!(self, ns, current)
            })
        }
    }
    
    func execute(command:Int, data:AnyObject?) {
        if let c:Command = Command(rawValue: command) {
            switch c {
            case .ToggleVisibility:
                let visibility:Bool = !viewState!.textVisibility;
                viewState = ViewControllerViewState(textVisibility: visibility)
            }
        }
    }
    
    func getViewState() -> ViewState {
        return viewState!;
    }
    
    func setViewState(vs: ViewState) {
        self.viewState = vs as? ViewControllerViewState
    }
    
    
    required init() {
        defer {
            self.viewState = ViewControllerViewState(textVisibility: true)
        }
    }
}
