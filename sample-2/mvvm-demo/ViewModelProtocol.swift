//
//  ViewModelProtocol.swift
//  mvvm-demo
//
//  Created by NGUYEN KHANH DUY on 5/17/16.
//  Copyright © 2016 NGUYEN KHANH DUY. All rights reserved.
//

import Foundation

protocol ViewState {}

protocol ViewModelProtocol: class {
    var onViewStateChanged: ((ViewModelProtocol, ViewState, ViewState?) -> ())? {get set}
    func getViewState() -> ViewState;
    func setViewState(viewstate: ViewState) -> Void;
    func execute(command:Int, data:AnyObject?) -> Void;
}