//
//  ViewController.swift
//  mvvm-demo
//
//  Created by NGUYEN KHANH DUY on 5/17/16.
//  Copyright © 2016 NGUYEN KHANH DUY. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var myButton: UIButton!
    @IBOutlet weak var myLabel: UILabel!
    
    var viewModel:ViewControllerViewModel! {
        didSet {
            self.viewModel.onViewStateChanged = {[unowned self] (viewModel, newViewState, oldViewState) in
                let v:ViewControllerViewState = newViewState as! ViewControllerViewState
                self.myLabel.hidden = !v.textVisibility
                self.myButton.setTitle(v.buttonText, forState: UIControlState.Normal)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel = ViewControllerViewModel();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onButtonClicked(sender: AnyObject) {
        viewModel.execute(ViewControllerViewModel.Command.ToggleVisibility.rawValue, data: NSNull())
    }
}

