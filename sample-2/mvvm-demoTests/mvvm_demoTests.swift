//
//  mvvm_demoTests.swift
//  mvvm-demoTests
//
//  Created by NGUYEN KHANH DUY on 5/17/16.
//  Copyright © 2016 NGUYEN KHANH DUY. All rights reserved.
//

import XCTest
@testable import mvvm_demo

class mvvm_demoTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSetViewState() {
        let viewModel = ViewControllerViewModel()
        XCTAssertNotNil(viewModel)
        let viewState = ViewControllerViewState(textVisibility: false) as ViewState
        viewModel.setViewState(viewState);
        
        let v:ViewControllerViewState = viewModel.getViewState() as! ViewControllerViewState
        XCTAssertFalse(v.textVisibility);
        XCTAssertTrue(v.buttonText == ViewControllerViewState.BTN_TEXT_SHOW);
    }
    
    func testToggleTextVisible() {
        var updateCounter = 0;
        
        var viewModel:ViewModelProtocol! {
            didSet {
                viewModel.onViewStateChanged = { (viewModel, newViewState, oldViewState) in
                    updateCounter += 1;
                    let _oldViewState: ViewState! = oldViewState
                    if _oldViewState != nil {
                        if (updateCounter == 2) {
                            let v:ViewControllerViewState = newViewState as! ViewControllerViewState
                            XCTAssertFalse(v.textVisibility);
                            XCTAssertTrue(v.buttonText == ViewControllerViewState.BTN_TEXT_SHOW);
                        } else if (updateCounter == 3) {
                            let v:ViewControllerViewState = newViewState as! ViewControllerViewState
                            XCTAssertTrue(v.textVisibility);
                            XCTAssertTrue(v.buttonText == ViewControllerViewState.BTN_TEXT_HIDE);
                        }
                    }
                }
            }
        }
        viewModel = ViewControllerViewModel()
        XCTAssertNotNil(viewModel)
        
        let v:ViewControllerViewState = viewModel.getViewState() as! ViewControllerViewState
        
        XCTAssertTrue(v.textVisibility);
        XCTAssertTrue(v.buttonText == ViewControllerViewState.BTN_TEXT_HIDE);
        
        viewModel.execute(ViewControllerViewModel.Command.ToggleVisibility.rawValue, data: NSNull())
        viewModel.execute(ViewControllerViewModel.Command.ToggleVisibility.rawValue, data: NSNull())
    }
    
}
